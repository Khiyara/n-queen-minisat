import sys
import os
import time

from logic import create_all_possible_line_into_cnf

NAME_FILE_MINISAT_CNF = "minisat_cnf"
OUTPUT_FILE_MINISAT = "result"
open(OUTPUT_FILE_MINISAT, 'w') ## trunk answer

def count_diagonals(N: int):
  memo = {}
  memo[1] = 1
  memo[2] = 2
  for i in range(3, N+1):
    memo[i] = ((i-1) * (i-2)) + (i * (i-1)) + memo[i-1]

  return memo[N]

def main():
  N = int(sys.argv[1])

  diagonals = count_diagonals(N)
  rows_and_columns = (N**2) * (N - 1)

  counter_clause = rows_and_columns + diagonals + N
  counter_variable = N * N

  file = open(NAME_FILE_MINISAT_CNF, "w+")

  cnf_init = "p cnf " + str(counter_variable) + " " + str(counter_clause) + "\n"
  file.write(cnf_init)

  create_all_possible_line_into_cnf(N, file)

  file.close()

  os.popen("minisat " + NAME_FILE_MINISAT_CNF + " " + OUTPUT_FILE_MINISAT)

  ### Wait until os.popen done
  while(1):
    result = open(OUTPUT_FILE_MINISAT, "r").read().splitlines()
    if(result != []):
      break

  ## There is solution
  if(result[0] == "SAT"):
    counter = 1
    for i in list(map(int, result[1].split(" "))):
      if i < 0:
        print('O', end=" ")
      else:
        print("X", end=" ")
      if(counter % N == 0):
        print()
      counter += 1
    ## TODO
    ## GUI

  ### No solution
  else:
    print("N-Queen problem with N x N board with N = " + str(N) +" is not satisfiable")

if __name__ == "__main__":
  main()
