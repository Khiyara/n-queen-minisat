import math

def create_all_possible_line_into_cnf(N: int, file):
  max_iter = N * N + 1
  # ALL CELL INIT
  for i in range(1, max_iter):
    file.write(str(i) + " ")
    if i % N == 0:
      file.write("0\n")

  # ROW PART
  for i in range(1, max_iter):
    for j in range(i + 1, math.ceil(i / N) * N + 1):
      #print(i , j)
      file.write(str(-i) +" "+ str(-j) + " 0\n")

  # COLUMN PART
  for i in range(1, max_iter):
    for j in range(i + N, max_iter, N):
      #print(i , j)
      file.write(str(-i) +" "+ str(-j) + " 0\n")

  # DIAGONAL LEFT TO RIGHT PART
  for i in range(1, max_iter):
    row = math.ceil(i / N)
    col = i % N
    if col == 0: col = N
    for j in range(i + N + 1, min(((N - col + row) * N + 1), max_iter), N + 1):
      #print(i, j)
      file.write(str(-i) +" "+ str(-j) + " 0\n")

  # DIAGONAL RIGHT TO LEFT PART
  for i in range(1, max_iter):
    for j in range(i + N - 1, max_iter, N - 1):
      if math.ceil((j - (N - 1)) / N) == math.ceil(j / N):
        break
      #print(i, j)
      file.write(str(-i) +" "+ str(-j) + " 0\n")














## DIAGONAL PARAT
 # i = 1
  # while(i < N + 1):
  #   j = i + N + 1
  #   rowTemp = N - i
  #   while (rowTemp > 0):
  #     print(str(i) + " " + str(j))
  #     file.write(str(-i) + " " + str(-j) +" 0\n")
  #     rowTemp -= 1
  #     j += N + 1
  #   i += 1

  # print()

  # row = N + 1
  # while(row < N * N + 1):
  #   col = row + N + 1
  #   rowTemp = (N * N // row)
  #   while (rowTemp > 0):
  #     print(str(row) + " " + str(col));
  #     rowTemp -= 1
  #     col += N + 1
  #   row += N;
